<?php

class Database {

	private $host;
	private $user;
	private $password;
	private $database;
	public	$connection;

	public function __construct() {
		$this->connect();
	}

	private function connect() {
		$this->host = 'localhost';
		$this->user = 'root';
		$this->password = '';
		$this->database = 'gjirafa50';

		$this->connection = new mysqli($this->host, $this->user, $this->password, $this->database);

		if ($this->connection->connect_errno) {
			echo "Failed to connect to MySQL: " . $this->connection->connect_error;
		}

		return $this->connection;
	}

	public function get($table) {
		$sql = sprintf(
			'SELECT * FROM %s',
			$table
		);

		return $this->connection->query($sql);
	}
}

?>