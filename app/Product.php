<?php

require __DIR__ . '/../core/Database.php';

class Product {

	private $products = array(); 
	private $database;
	
	public function __construct() {
		$this->database = new Database();
	}

	public function getProducts() {
		$result = $this->database->get('products');

		while($product = $result->fetch_assoc()) {
			array_push($this->products, $product);
		}

		$remaining = (int)$this->getDeliveryTime();

		return require __DIR__ . '/../views/products.view.php';
	}

	public function getDeliveryTime() {
		$currentDate = new DateTime('now', new DateTimeZone('Europe/Amsterdam'));
		$currentDay = $currentDate->format('D');
		$currentHour = $currentDate->format('H');

		$deliveryDate = new DateTime();

		if(($currentDay 	== 'Fri' && $currentHour >= 21) || 
			$currentDay 	== 'Sat' || 
			$currentDay 	== 'Sun' || 
			$currentDay 	== 'Mon' || 
			($currentDay	== 'Tue' && $currentHour < 21)) {

			$deliveryDate->modify('next saturday');
		} else {
			$deliveryDate->modify('next wednesday');
		}

		return date_diff($currentDate, $deliveryDate)->format('%a');
	}
}

?>