<?php require('partials/head.view.php'); ?>

<div class="container">
	<div class="row">
		<?php foreach($this->products as $product) { ?>
		<div class="col-3">
			<div class="card my-5">
				<img class="card-img-top" src="../public/images/<?= $product['image']; ?>" alt="">
				<div class="card-body">
					<h5 class="card-title"><?= $product['name']; ?></h5>
					<p class="card-text"><?= $product['description']; ?></p>
					<p class="card-text"><p class="text-muted">Delivers in <b><?= $remaining; ?></b> days</p></p>
				</div>
			</div>
		</div>
		<?php } ?>
	</div>
</div>

<?php require('partials/footer.view.php'); ?>